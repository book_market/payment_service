package service

import (
	"context"
	"database/sql"
	"errors"

	"github.com/sirupsen/logrus"
	pbp "gitlab.com/book_market/payment_service/genproto/payment_service"
	"gitlab.com/book_market/payment_service/storage"
	"gitlab.com/book_market/payment_service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type TransactionService struct {
	pbp.UnimplementedTransactionServiceServer
	storage  storage.StorageI
	inMemory storage.InMemoryStorageI
	logger   *logrus.Logger
}

func NewTransactionService(strg storage.StorageI, inMemory storage.InMemoryStorageI, log *logrus.Logger) *TransactionService {
	return &TransactionService{
		storage:  strg,
		inMemory: inMemory,
		logger:   log,
	}
}

func (t *TransactionService) Create(ctx *context.Context, req *pbp.Transaction) (*pbp.Transaction, error) {
	transaction, err := t.storage.Transaction().CreateTransaction(&repo.Transaction{
		StripeCheckoutSessionID: req.StripeId,
		Amount:                  float64(req.Amount),
		OrderID:                 req.OrderId,
		Status:                  req.Status,
	})
	if err != nil {
		t.logger.WithError(err).Error("failed to create transaction")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseTransactionModel(transaction), nil
}

func parseTransactionModel(transaction *repo.Transaction) *pbp.Transaction {
	return &pbp.Transaction{
		Id: transaction.ID,
		StripeId: transaction.StripeCheckoutSessionID,
		Amount: float32(transaction.Amount),
		OrderId: transaction.OrderID,
		Status: transaction.Status,
		CreatedAt: transaction.CreatedAt.String(),
	}
}

func (t *TransactionService) Get(ctx context.Context, req *pbp.TransactionIdRequest) (*pbp.Transaction, error) {
	transaction, err := t.storage.Transaction().GetTransaction(req.Id)
	if err != nil {
		t.logger.WithError(err).Error("failed to get transaction by id")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseTransactionModel(transaction), nil
}

func (t *TransactionService) UpdateStatus(ctx context.Context, req *pbp.UpdateTransactionStatus) error {
	err := t.storage.Transaction().UpdateStatus(&repo.UpdateTransactionStatus{
		StripeCheckoutSessionID: req.StripeId,
		Status:                  req.Status,
	})
	if err != nil {
		t.logger.WithError(err).Error("failed to create transaction")
		return status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return nil
}